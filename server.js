var config = require('./server/config'),
    express = require('express'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    User = require('./server/modules/user.module');

/* Create App */
var app = express();

/* Allow CORS */
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

/* Body Parser Middleware */
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

/* Session Middleware */
app.use(session({
    secret: 'sessionsecret',
    saveUninitialized: false,
    resave: false
}));

/* Define Endpoints */
app.post('/login', function (req, res) {
    var credentials = req.body;
    new User().authenticate(credentials).then(function (isValid) {
        if (isValid) {
            req.session.loggedIn = true;
            req.session.save(function () {
                res.status(200).send(req.sessionID);
            });
        } else {
            res.sendStatus(404);
        }
    }).catch(function (err) {
        res.sendStatus(500);
    });
});
app.post('/logout', function (req, res) {
    req.session.destroy();
    res.sendStatus(200);
});

/* Start Server */
app.listen(config.server.port);
console.log('API server listening on', config.server.port);