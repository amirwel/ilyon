angular.module('app')
    .controller('loginCtrl', loginCtrl);

loginCtrl.$inject = ['$scope', '$location', 'AuthService', 'UserService', 'md5'];

function loginCtrl($scope, $location, AuthService, UserService, md5) {
    $scope.login = function () {
        $scope.errorMessag = '';
        AuthService.authenticate({
            username: $scope.username,
            password: md5.createHash($scope.password)
        }).then(function (res) {
            UserService.save($scope.username, res.data);
            $location.path('/');
        }, function (res) {
            if (res.status == 404) {
                $scope.errorMessage = 'Inccorrect username or password';
            } else {
                $scope.errorMessage = 'Unable to log in, please try again.'
            }
        });
    };
}