angular.module('app')
    .controller('logoutCtrl', logoutCtrl);

logoutCtrl.$inject = ['$scope', '$location', 'AuthService', 'UserService'];

function logoutCtrl($scope, $location, AuthService, UserService) {
    AuthService.logout().then(function () {
        UserService.remove();
        $location.path('/login');
    });
}