angular.module('app')
    .controller('mainCtrl', mainCtrl);

mainCtrl.$inject = ['$scope', 'UserService'];

function mainCtrl($scope, UserService) {
    $scope.username = UserService.get();
}