angular.module('app')
    .config(config);

config.$inject = ['$routeProvider', '$locationProvider'];

function config($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/main.view.html',
            controller: 'mainCtrl'
        })
        .when('/login', {
            templateUrl: 'views/login.view.html',
            controller: 'loginCtrl'
        })
        .when('/logout', {
            templateUrl: 'views/logout.view.html',
            controller: 'logoutCtrl'
        })
        .otherwise({ redirectTo: '/' });
    $locationProvider.html5Mode(true);
}