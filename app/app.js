angular.module('app', [
    'templates',
    'config',
    'ngRoute',
    'ngCookies',
    'angular-md5'
]).run(run);

run.$inject = ['$rootScope', '$location', 'UserService'];

function run($rootScope, $location, UserService) {
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        if (!UserService.get()) {
            $rootScope.$applyAsync(function () {
                $location.path('/login');
            });
        }
    });
}