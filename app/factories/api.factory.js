angular.module('app')
    .factory('APIFactory', APIFactory);

APIFactory.$inject = ['$http', 'API'];

function APIFactory($http, API) {
    return {
        buildRequestUri: function (param) {
            return window.location.protocol + '//' + API.host + ':' + API.port + '/' + param
        }
    };
}