var app = angular.module('app');
app.service('AuthInterceptor', AuthInterceptor);
AuthInterceptor.$inject = ['$q', '$location'];
function AuthInterceptor($q, $location) {
    this.responseError = function (res) {
        if (res.status == 401) {
            $location.path('/login');
        }
        return $q.reject(res);
    };
}
app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
}]);
