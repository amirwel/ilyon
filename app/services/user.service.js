angular.module('app')
    .service('UserService', UserService);

UserService.$inject = ['$cookies'];

function UserService($cookies) {
    this.save = function (username, sid) {
        $cookies.put('sid', sid);
        $cookies.put('user', username);
    };
    this.remove = function () {
        $cookies.remove('user');
    };
    this.get = function () {
        return $cookies.get('user')
    };
}