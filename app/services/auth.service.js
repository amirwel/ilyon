angular.module('app')
    .service('AuthService', AuthService);

AuthService.$inject = ['$http', 'APIFactory'];

function AuthService($http, APIFactory) {
    this.authenticate = function (credentials) {
        return $http.post(APIFactory.buildRequestUri('login'), credentials);
    };
    this.logout = function () {
        return $http.post(APIFactory.buildRequestUri('logout'));
    };
}