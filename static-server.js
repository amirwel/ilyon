var StaticServer = require('static-server');
var server = new StaticServer({
  rootPath: './',            // required, the root of the server file tree 
  name: 'static-server',   // optional, will set "X-Powered-by" HTTP header 
  port: 80,               // optional, defaults to a random port 
  host: 'localhost',       // optional, defaults to any interface 
  cors: '*',                 // optional, defaults to undefined 
  followSymlink: true,      // optional, defaults to a 404 error 
  templates: {
    index: 'index.html',      // optional, defaults to 'index.html' 
    notFound: 'index.html'    // optional, defaults to undefined 
  }
});

server.start(function () {
  console.log('Static files server listening on', server.port);
});