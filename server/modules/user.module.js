var userModel = require('../models/user.model');

function User() {
    this.authenticate = function (credentials) {
        return new Promise(function (reslove, reject) {
            userModel.findOne({
                username: credentials.username,
                password: credentials.password
            }).then(function (doc) {
                reslove(doc != null);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
}

module.exports = User;