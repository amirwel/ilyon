var config = require('../config'),
    mongoose = require('mongoose');

mongoose.Promise = Promise;

mongoose.connect(config.mongo.uri, config.mongo.options, function (err) {
    if (err) {
        console.debug('MONGO ERROR:', err);
    }
});

module.exports = mongoose;