module.exports = {
    server: {
        port: 8080
    },
    mongo: {
        uri: 'mongodb://localhost/ilyon?ssl=true',
        options: {
            useMongoClient: true,
            keepAlive: true
        }
    }
};