var mongo = require('../modules/mongo.module'),
    Schema = mongo.Schema;

var userSchema = new Schema({
    username: { type: String, required: true, unique: true },
    Password: { type: String, required: true, unique: false }
});
var userModel = mongo.model('users', userSchema)


module.exports = userModel;