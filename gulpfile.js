'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    concat = require('gulp-concat'),
    gap = require('gulp-append-prepend'),
    sass = require('gulp-sass'),
    gulpNgConfig = require('gulp-ng-config'),
    templateCache = require('gulp-angular-templatecache');

const PATH = {
    APP: 'app/',
    ASSETS: 'assets/',
    VENDOR: 'node_modules/'
},
    FILES = {
        VENDOR: [
            PATH.VENDOR + 'angular/angular.min.js',
            PATH.VENDOR + 'angular-route/angular-route.min.js',
            PATH.VENDOR + 'angular-cookies/angular-cookies.min.js',
            PATH.VENDOR + 'angular-md5/angular-md5.min.js'
        ],
        APP: {
            SCRIPT: PATH.APP + '**/*.js',
            TEMPLATE: PATH.APP + '**/*html',
            STYLE: PATH.APP + 'style/style.scss',
            CONFIG: PATH.APP + '/*.config.json'
        }
    };

gulp.task('vendor', function () {
    return gulp.src(FILES.VENDOR)
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(PATH.ASSETS));
});

gulp.task('config', function () {
    gulp.src(FILES.APP.CONFIG)
        .pipe(gulpNgConfig('config'))
        .pipe(gulp.dest(PATH.ASSETS));
});

gulp.task('app', function () {
    gulp.src(FILES.APP.STYLE)
        .pipe(sass())
        .pipe(concat('style.css'))
        .pipe(gulp.dest(PATH.ASSETS));

    gulp.src(FILES.APP.TEMPLATE)
        .pipe(templateCache('templates.js', { standalone: true }))
        .pipe(gulp.dest(PATH.ASSETS));

    return gulp.src(FILES.APP.SCRIPT)
        .pipe(concat('app.js'))
        .pipe(gap.prependText('"use strict"; (function(){'))
        .pipe(gap.appendText('})();'))
        .pipe(gulp.dest(PATH.ASSETS));
});

gulp.task('watch', function () {
    return watch(PATH.APP, function () {
        gulp.start('app');
    });
});

gulp.task('build', function () {
    gulp.start('vendor');
    gulp.start('config');
    gulp.start('app');
});